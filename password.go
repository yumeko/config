// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

import (
	"bytes"
	"encoding/base64"

	ini "gopkg.in/ini.v1"
)

type Password struct {
	nsbs      []NodeStringOrBytes
	hashFn    func(string) ([]byte, error)
	password  inikv
	hash      inikv
	hashValue []byte
}

func (p *Password) Reload() (errs []error) {
	for _, nsb := range p.nsbs {
		errs = append(errs, nsb.Reload()...)
	}
	return
}

func (p *Password) Update(nini *ini.File) (changed bool, errs []error) {
	psec := nini.Section(p.password.Section)
	pkey, err := psec.GetKey(p.password.Key)
	if err != nil {
		pkey, _ = psec.NewKey(p.password.Key, p.password.Default)
		pkey.Comment = p.password.Comment
		changed = true
	}
	pvalue := pkey.Value()
	hsec := nini.Section(p.hash.Section)
	hkey, err := hsec.GetKey(p.hash.Key)
	if err != nil {
		hkey, _ = hsec.NewKey(p.hash.Key, p.hash.Default)
		hkey.Comment = p.hash.Comment
		changed = true
	}
	var hash []byte
	if pvalue != "" {
		hash, err = p.hashFn(pvalue)
		if err != nil {
			errs = append(errs, err)
		} else {
			pkey.SetValue("")
			hkey.SetValue(base64.URLEncoding.EncodeToString(hash))
			changed = true
		}
	} else {
		hash, err = base64.URLEncoding.DecodeString(hkey.Value())
	}
	if !bytes.Equal(hash, p.hashValue) {
		for _, n := range p.nsbs {
			errs = append(errs, NodeStringOrBytesUpdate(n, hash)...)
		}
	}
	return
}

// NewPassword creates a NodeINI for saving passwords.
// Template should have a password field first, followed by
// a password hash field. If password is filled it will call the
func NewPassword(template string, hashPassword func(password string) (hashed []byte, err error), nsbs ...NodeStringOrBytes) *Password {
	for _, nsb := range nsbs {
		err := NodeStringOrBytesCheck(nsb)
		if err != nil {
			panic(err)
		}
	}
	p := &Password{
		nsbs:   nsbs,
		hashFn: hashPassword,
	}
	nini, err := ini.Load(([]byte)(template))
	if err != nil {
		panic(err)
	}
	found := 0
sections:
	for _, sec := range nini.Sections() {
		keys := sec.Keys()
		for _, k := range keys {
			ikv := &p.password
			if found > 0 {
				ikv = &p.hash
			}
			ikv.Key = k.Name()
			ikv.Section = sec.Name()
			ikv.Default = k.Value()
			ikv.Comment = k.Comment
			found++
			if found >= 2 {
				break sections
			}
		}
	}
	if found != 2 {
		panic("Could not find keys in template")
	}
	return p
}
