// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

import (
	"io/ioutil"
	"os"
)

func NewFile(children ...NodeString) *File {
	return &File{
		Children: children,
	}
}

//File is a NodeStream that opens a file outputing to child NodeStrings. Should be behind a MTime
type File struct {
	Children []NodeString
}

func (i *File) Attach(n NodeString) {
	i.Children = append(i.Children, n)
}

func (i *File) Reload() (errs []error) {
	for _, c := range i.Children {
		errs = append(errs, c.Reload()...)
	}
	return
}

func (i *File) Update(finame string) (errs []error) {
	fi, err := os.Open(finame)
	if err != nil {
		return []error{err}
	}
	defer fi.Close()
	bval, err := ioutil.ReadAll(fi)
	if err != nil {
		return []error{err}
	}
	val := string(bval)
	for _, c := range i.Children {
		errs = append(errs, c.Update(val)...)
	}
	return
}
