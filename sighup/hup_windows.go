// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

//+build windows

package sighup

import "gitlab.com/yumeko/config"

//EnableSigHUP starts listening for a SIGHUP(on !windows)
func EnableSigHUP(n config.NodeNil, panicOnError bool) {
}
