// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

//+build !windows

//Package sighup provides a SIGHUP listener on !windows
package sighup

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/yumeko/config"
)

//EnableSigHUP starts listening for a SIGHUP(on !windows)
func EnableSigHUP(n config.NodeNil, panicOnError bool) {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGHUP)
	go func() {
		for {
			<-ch
			errs := n.Reload()
			if errs != nil {
				if len(errs) > 0 {
					fmt.Println("Fatal error:")
					for _, err := range errs {
						fmt.Println(err)
					}
					if panicOnError {
						panic("Fatal Error reloading")
					}
				}
			}
		}
	}()
}
