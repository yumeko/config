// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/yumeko/errors"

	ini "gopkg.in/ini.v1"
)

type inikv struct {
	Section string
	Key     string
	Default string
	Comment string
}

type kvpair struct {
	inikv
	Value string
	Nodes []NodeString
}

func (k *kvpair) Update(nini *ini.File) (bool, []error) {
	changed := false
	nsec := nini.Section(k.Section)
	nkey, err := nsec.GetKey(k.Key)
	value := k.Default
	if err != nil {
		nkey, _ = nsec.NewKey(k.Key, value)
		nkey.Comment = k.Comment
		changed = true
	} else {
		value = nkey.Value()
	}
	if k.Value != value {
		errs := []error{}
		for _, n := range k.Nodes {
			ierrs := n.Update(value)
			for i := range ierrs {
				ierrs[i] = errors.At(ierrs[i], fmt.Sprintf("[%v]%v", k.Section, k.Key))
			}
			errs = append(errs, ierrs...)
		}
		return changed, errs
	}
	return changed, []error{}
}

func (k *kvpair) Reload() (errs []error) {
	for _, n := range k.Nodes {
		errs = append(errs, n.Reload()...)
	}
	return
}

func NewINI() *INI {
	return &INI{}
}

//INI is a NodeString with NodeString children. It should be attached to a MTime or similar
type INI struct {
	children []NodeINI
}

type NodeINI interface {
	//Update is always called if Update is called on its parent. return true if the File has changed
	Update(*ini.File) (bool, []error)
	Reload() []error
}

// Attach takes a NodeString and a template INI chunk:
// 	ini.Attach(Ptr(&httpHostPort),`
// 		[HTTP]
// 		# host:port for the HTTP server to listen on
// 		http = :80
// 	`)
func (i *INI) Attach(template string, n ...NodeString) {
	k := &kvpair{
		Nodes: n,
	}
	k.Default = "\000\000"
	dini, err := ini.Load(([]byte)(template))
	if err != nil {
		panic(err)
	}
	for _, sec := range dini.Sections() {
		keys := sec.Keys()
		if len(keys) > 0 {
			k.Key = keys[0].Name()
			k.Section = sec.Name()
			k.Default = keys[0].Value()
			k.Comment = keys[0].Comment
			break
		}
	}
	i.AttachRaw(k)
}

// AttachRaw allows you to make arbitrary modifications to the ini at reload-time
func (i *INI) AttachRaw(n NodeINI) {
	i.children = append(i.children, n)
}

func (i *INI) Reload() (errs []error) {
	for _, c := range i.children {
		errs = append(errs, c.Reload()...)
	}
	return
}

func (i *INI) Update(finame string) (errs []error) {
	err := func() error {
		fi, err := os.OpenFile(finame, os.O_CREATE|os.O_RDWR, 0770)
		if err != nil {
			return err
		}
		defer fi.Close()
		fibytes, err := ioutil.ReadAll(fi)
		if err != nil {
			return err
		}
		nini, err := ini.Load(fibytes)
		if err != nil {
			return err
		}
		changed := false
		for _, c := range i.children {
			ichanged, ierrs := c.Update(nini)
			changed = changed || ichanged
			errs = append(errs, ierrs...)
		}
		if changed {
			_, err := fi.Seek(0, os.SEEK_SET)
			if err != nil {
				return err
			}
			n, err := nini.WriteTo(fi)
			err2 := fi.Truncate(n)
			if err != nil {
				return err
			}
			if err2 != nil {
				return err2
			}
		}
		return nil
	}()
	if err != nil {
		return []error{err}
	}
	return
}
