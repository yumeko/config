// +build none

package main

import (
	"fmt"
	"os"

	"gitlab.com/yumeko/config"
	"gitlab.com/yumeko/config/sighup"
)

var configINI = config.NewINI()
var configINIPath = config.NewString("template.ini", config.NewMTime(configINI))
var configRoot = config.NewInstance(configINIPath)

func main() {
	if len(os.Args) > 1 {
		configINIPath.Change(os.Args[1])
	}
	errs := configRoot.Reload()
	if len(errs) > 0 {
		fmt.Println("Fatal error:")
		for _, err := range errs {
			fmt.Println(err)
		}
		return
	}
	sighup.EnableSigHUP(configRoot, true)

	//...
}
