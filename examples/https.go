// +build none

package main

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"os/signal"

	"gitlab.com/yumeko/config"
	"gitlab.com/yumeko/config/listener"
	"gitlab.com/yumeko/config/sighup"
)

var configINI = config.NewINI()
var configINIPath = config.NewString("https.ini", config.NewMTime(configINI))
var configRoot = config.NewInstance(configINIPath)
var Mux = http.DefaultServeMux

func init() {
	configINI.Attach(`[http]
# [host]:port to listen on for non secure http connections
http = :80`, configlistener.NewNet("tcp", true, func() (configlistener.Server, error) {
		return &http.Server{}, nil
	}))
	https := configlistener.NewTLS(true, func() (configlistener.Server, *tls.Config, error) {
		return &http.Server{}, &tls.Config{}, nil
	})
	configINI.Attach(`[http]
# [host]:port to listen on for secure http connections
https = :443`, https.HostPort)
	configINI.Attach(`[http]
# name of the certificate file for https
https_cert = https.crt`, config.NewMTime(https.CertFile))
	configINI.Attach(`[http]
# name of the certificate key file for https
https_keyt = https.key`, config.NewMTime(https.KeyFile))
}

func main() {
	if len(os.Args) > 1 {
		configINIPath.Change(os.Args[1])
	}
	errs := configRoot.Reload()
	if len(errs) > 0 {
		fmt.Println("Fatal error:")
		for _, err := range errs {
			fmt.Println(err)
		}
		return
	}
	sighup.EnableSigHUP(configRoot, true)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello World"))
	})
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}
