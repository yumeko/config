yumeko/config
=============

[![GoDoc](https://godoc.org/gitlab.com/yumeko/config?status.svg)](https://godoc.org/gitlab.com/yumeko/config)

Package config provides a framework for reloadable config values.

Points of configuration are presented as a directed acyclic graph. Calling Reload on a node will call reload on all of its dependants. Additionally reload will check for a config change, updating its values and passing them to dependants. This allows for complex state changes to cascade to all required values easily.

Example
-------
Read a ini file, and print the value whenever it changes

	package main

	import (
		"fmt"
		"os"
		"os/signal"

		"gitlab.com/yumeko/config"
		"gitlab.com/yumeko/config/sighup"
	)

	var configINI = config.NewINI()
	var configINIPath = config.NewString("basic.ini", config.NewMTime(configINI))
	var configRoot = config.NewInstance(configINIPath)

	func init() {
		configINI.Attach(`[section]
	# comment
	key=value`, config.NewPointer(func(v string) error {
			fmt.Printf("New value is %v\n", v)
			return nil
		}))
	}

	func main() {
		if len(os.Args) > 1 {
			configINIPath.Change(os.Args[1])
		}
		errs := configRoot.Reload()
		if len(errs) > 0 {
			fmt.Println("Fatal error:")
			for _, err := range errs {
				fmt.Println(err)
			}
			return
		}
		sighup.EnableSigHUP(configRoot, true)
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c
	}
