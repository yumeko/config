// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

func NewCombine(Deps int, Update func() error) *Combine {
	return &Combine{
		Total:   Deps,
		Updated: Update,
	}
}

// Combine is a NodeString that calls Update when any of its dependancys have called upstream.
// It requires a count of dependancys to check when its done.
type Combine struct {
	Total   int
	Updated func() error
	changed bool
	num     int
}

func (c *Combine) Update(string) []error {
	c.changed = true
	return nil
}

func (c *Combine) Reload() []error {
	if c.changed {
		c.num++
		if c.num == c.Total {
			c.num = 0
			c.changed = false
			err := c.Updated()
			if err != nil {
				return []error{err}
			}
		}
	}
	return nil
}
