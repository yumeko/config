// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package configlistener

import (
	"context"
	"crypto/tls"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net"
	"path/filepath"

	"gitlab.com/yumeko/config"
	"golang.org/x/crypto/pkcs12"
)

type ConfigTLS struct {
	HostPort config.NodeString
	CertFile config.NodeString
	KeyFile  config.NodeString
}

type tlsvalue struct {
	value   string
	changed bool
	tls     *tlsListener
}

func (c *tlsvalue) Update(val string) []error {
	c.value = val
	c.changed = true
	return nil
}

func (c *tlsvalue) Reload() []error {
	if c.changed {
		err := c.tls.reload()
		if err != nil {
			return []error{err}
		}
	}
	return nil
}

type tlsListener struct {
	newserver   func() (Server, *tls.Config, error)
	hostport    tlsvalue
	cert        tlsvalue
	key         tlsvalue
	s           Server
	l           net.Listener
	disableable bool
}

func (t *tlsListener) reload() error {
	t.hostport.changed = false
	t.cert.changed = false
	t.key.changed = false
	shutdown := t.disableable
	os := t.s
	defer func() {
		if shutdown {
			if os != nil {
				if t.s == os {
					t.s = nil
				}
				go os.Shutdown(context.Background())
			}
		}
	}()
	if t.hostport.value == "" {
		if t.disableable {
			return nil
		}
		return fmt.Errorf("host/port must be set")
	}
	if t.cert.value == "" {
		if t.disableable {
			return nil
		}
		return fmt.Errorf("certificate must be set")
	}
	ns, tlsc, err := t.newserver()
	if err != nil {
		return err
	}
	t.s = ns
	// Load the cert
	cext := filepath.Ext(t.cert.value)
	if cext == ".p12" || cext == ".pfx" {
		bytes, err := ioutil.ReadFile(t.cert.value)
		if err != nil {
			return err
		}
		pems, err := pkcs12.ToPEM(bytes, t.key.value)
		if err != nil {
			return err
		}
		b := []byte{}
		for _, p := range pems {
			b = append(b, pem.EncodeToMemory(p)...)
		}
		cert, err := tls.X509KeyPair(b, b)
		if err != nil {
			return err
		}
		tlsc.Certificates = append(tlsc.Certificates, cert)
	} else {
		cbytes, err := ioutil.ReadFile(t.cert.value)
		if err != nil {
			return err
		}
		pbytes := cbytes
		if t.key.value != "" {
			pbytes, err = ioutil.ReadFile(t.key.value)
			if err != nil {
				return err
			}
		}
		cert, err := tls.X509KeyPair(cbytes, pbytes)
		if err != nil {
			return err
		}
		tlsc.Certificates = append(tlsc.Certificates, cert)
	}
	// listener time
	if t.l != nil {
		t.l.Close()
	}
	t.l, err = net.Listen("tcp", t.hostport.value)
	if err != nil {
		return err
	}
	shutdown = true
	go t.s.Serve(tls.NewListener(t.l, tlsc))
	return nil
}

func NewTLS(disableable bool, newserver func() (Server, *tls.Config, error)) ConfigTLS {
	t := &tlsListener{
		newserver:   newserver,
		disableable: disableable,
	}
	t.hostport.tls = t
	t.cert.tls = t
	t.key.tls = t
	return ConfigTLS{
		HostPort: &t.hostport,
		CertFile: &t.cert,
		KeyFile:  &t.key,
	}
}
