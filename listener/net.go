// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

// Package configlistener provides a nice interface for TLS and HTTP servers
// with hot reloading certs, certpaths, and ports.
package configlistener

import (
	"context"
	"fmt"
	"net"

	"gitlab.com/yumeko/config"
)

type Server interface {
	Serve(net.Listener) error
	Shutdown(context.Context) error
}

func NewNet(nettype string, disableable bool, newserver func() (Server, error)) config.NodeString {
	var s Server
	var l net.Listener
	return config.NewPointer(func(hostport string) error {
		shutdown := disableable
		os := s
		defer func() {
			if shutdown {
				if os != nil {
					if s == os {
						s = nil
					}
					go os.Shutdown(context.Background())
				}
			}
		}()
		if hostport == "" {
			if disableable {
				return nil
			}
			return fmt.Errorf("host/port must be set")
		}
		var err error
		s, err = newserver()
		if err != nil {
			return err
		}
		if l != nil {
			l.Close()
		}
		l, err = net.Listen(nettype, hostport)
		if err != nil {
			return err
		}
		shutdown = true
		go s.Serve(l)
		return nil
	})
}
