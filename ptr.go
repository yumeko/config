// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

import (
	"fmt"
	"net/url"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// Pointer is a NodeString that updates a
// 	- chan T
// 	- *T
// 	- func(T)
// 	- func(T) error
// T may be:
// 	- u?int(|ptr|8|16|32|64)
// 	- float(32|64)
// 	- []byte
// 	- string
// 	- bool
// 	- time.Duration
// 	- time.Time
type Pointer struct {
	update func(string) error
}

func (i *Pointer) Reload() []error {
	return nil
}

func (i *Pointer) Update(value string) []error {
	err := i.update(value)
	if err != nil {
		return []error{err}
	}
	return nil
}

var errType = reflect.TypeOf((*error)(nil)).Elem()

func nilErr(fn func(reflect.Value)) func(reflect.Value) error {
	return func(v reflect.Value) error {
		fn(v)
		return nil
	}
}

func NewPointer(val interface{}) *Pointer {
	rval := reflect.ValueOf(val)
	var typ reflect.Type
	var fn func(reflect.Value) error
	switch rval.Kind() {
	case reflect.Chan:
		typ = rval.Type().Elem()
		fn = nilErr(rval.Send)
	case reflect.Ptr:
		typ = rval.Type().Elem()
		fn = nilErr(rval.Elem().Set)
	case reflect.Func:
		t := rval.Type()
		out := t.NumOut()
		if !(t.NumIn() == 1 && (out == 0 || (out == 1 && t.Out(0).AssignableTo(errType)))) {
			panic(fmt.Errorf("Expected %v to have signature be func(T, error) error, or func() error", t))
		}
		typ = t.In(0)
		if out == 1 {
			fn = func(v reflect.Value) error {
				orerr := rval.Call([]reflect.Value{v})
				err, _ := orerr[0].Interface().(error)
				return err
			}
		} else {
			fn = func(v reflect.Value) error {
				rval.Call([]reflect.Value{v})
				return nil
			}
		}
	default:
		panic(fmt.Errorf("%v is not a pointer, channel, or func", rval.Type()))
	}
	var sfn func(string) error
	switch typ.Kind() {
	case reflect.Bool:
		sfn = func(s string) error {
			switch strings.ToLower(s) {
			case "1", "t", "true", "yes", "y", "on", "enabled":
				return fn(reflect.ValueOf(true))
			case "0", "f", "false", "no", "n", "off", "disabled":
				return fn(reflect.ValueOf(false))
			default:
				return fmt.Errorf("%v is not a bool", s)
			}
		}
	case reflect.Int:
		sfn = pint(typ, fn, 0)
	case reflect.Int8:
		sfn = pint(typ, fn, 8)
	case reflect.Int16:
		sfn = pint(typ, fn, 16)
	case reflect.Int32:
		sfn = pint(typ, fn, 32)
	case reflect.Int64:
		if typ.AssignableTo(reflect.TypeOf(time.Duration(0))) {
			sfn = func(s string) (err error) {
				dur, err := time.ParseDuration(s)
				if err != nil {
					return err
				}
				return fn(reflect.ValueOf(dur))
			}
		} else {
			sfn = pint(typ, fn, 64)
		}
	case reflect.Uint:
		sfn = puint(typ, fn, 0)
	case reflect.Uint8:
		sfn = puint(typ, fn, 8)
	case reflect.Uint16:
		sfn = puint(typ, fn, 16)
	case reflect.Uint32:
		sfn = puint(typ, fn, 32)
	case reflect.Uint64:
		sfn = puint(typ, fn, 64)
	case reflect.Float32:
		sfn = pfloat(typ, fn, 32)
	case reflect.Float64:
		sfn = pfloat(typ, fn, 64)
	case reflect.Slice:
		if typ.Elem().Kind() != reflect.Uint8 {
			panic(fmt.Errorf("%v is not a valid type", typ))
		}
		sfn = func(s string) error {
			return fn(reflect.ValueOf(([]byte)(s)))
		}
	case reflect.String:
		sfn = func(s string) error {
			return fn(reflect.ValueOf(s))
		}
	case reflect.Ptr:
		if typ.AssignableTo(reflect.TypeOf(&url.URL{})) {
			sfn = func(s string) error {
				u, err := url.Parse(s)
				if err != nil {
					return err
				}
				return fn(reflect.ValueOf(u))
			}
		}
	}
	if sfn == nil {
		panic(fmt.Errorf("%v is not a valid type", typ))
	}
	return &Pointer{
		update: sfn,
	}
}

var descore = strings.NewReplacer(
	" ", "",
	"_", "",
	",", "",
)

//allow numbers with underscores,(toml style) and SI prefixes (for bytes)
func reint(src string) (str string, mul int, err error) {
	mul = 1
	str = src
	lastnum := strings.LastIndexAny(str, "0123456789")
	sfx := ""
	if lastnum > 0 {
		sfx = strings.ToLower(str[lastnum+1:])
		str = str[:lastnum+1]
	}
	if len(sfx) > 0 {
		imul := 0
		if (len(sfx) == 3 && sfx[1:] == "ib") || len(sfx) == 1 {
			imul = 1000
		} else if len(sfx) == 2 && sfx[1] == 'b' {
			imul = 1024
		}
		switch sfx[0] {
		case 'p':
			imul *= imul
			fallthrough
		case 't':
			imul *= imul
			fallthrough
		case 'g':
			imul *= imul
			fallthrough
		case 'm':
			imul *= imul
			fallthrough
		case 'k':
		default:
			imul = 0
		}
		if imul == 0 {
			err = fmt.Errorf("'%v' is not a valid suffix (/[ptgmk](b|ib|)/i)", sfx)
		} else {
			mul = imul
		}
	}
	return
}
func pint(typ reflect.Type, update func(reflect.Value) error, bit int) func(string) error {
	return func(src string) error {
		src, mul, err := reint(descore.Replace(src))
		if err != nil {
			return err
		}
		i, err := strconv.ParseInt(src, 0, bit)
		if err != nil {
			return err
		}
		val := reflect.New(typ).Elem()
		val.SetInt(i * int64(mul))
		return update(val)
	}
}
func puint(typ reflect.Type, update func(reflect.Value) error, bit int) func(string) error {
	return func(src string) error {
		src, mul, err := reint(descore.Replace(src))
		if err != nil {
			return err
		}
		i, err := strconv.ParseUint(src, 0, bit)
		if err != nil {
			return err
		}
		val := reflect.New(typ).Elem()
		val.SetUint(i * uint64(mul))
		return update(val)
	}
}
func pfloat(typ reflect.Type, update func(reflect.Value) error, bit int) func(string) error {
	return func(src string) error {
		src, mul, err := reint(descore.Replace(src))
		if err != nil {
			return err
		}
		i, err := strconv.ParseFloat(src, bit)
		if err != nil {
			return err
		}
		val := reflect.New(typ).Elem()
		val.SetFloat(i * float64(mul))
		return update(val)
	}
}
