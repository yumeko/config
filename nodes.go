// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

//Package config provides a framework for reloadable config values.
//
// Points of configuration are presented as a directed acyclic graph.
// Calling Reload on a node will call reload on all of its dependants.
// Additionally reload will check for a config change, updating its values
// and passing them to dependants. This allows for complex state changes to
// cascade to all required values easily.
package config

import (
	"fmt"
	"reflect"
)

// Node represents a node in a directecd acyclic graph
// NodeT should implement a Update(t T) []error method
// Update should only be called when t is changed.
// Reload should always be passed along to children
// Nodes may implement an Attach(NodeX) method to attach children nodes
type Node interface {
	Reload() []error
}

//NodeNil is a Node<nil>
type NodeNil interface {
	Node
}

//NodeString is a Node<string>
type NodeString interface {
	Node
	Update(string) []error
}

//NodeBytes is a Node<[]byte>
type NodeBytes interface {
	Node
	Update([]byte) []error
}

//NodeStringOrByte is a Node<string> that also accepts a Node<[]byte>
type NodeStringOrBytes interface {
	Node
}

// NodeStringOrBytesCheck checks a NodeStringOrBytes to ensure its valid.
// If this returns nil NodeStringOrBytesUpdate won't panic
func NodeStringOrBytesCheck(nsb NodeStringOrBytes) error {
	update := reflect.ValueOf(nsb).MethodByName("Update")
	if !update.IsValid() {
		return fmt.Errorf("Type %T missing Update method", nsb)
	}
	t := update.Type()
	if t.NumIn() == 1 &&
		t.NumOut() == 1 &&
		t.Out(0).AssignableTo(reflect.TypeOf([]error{})) &&
		(t.In(0).AssignableTo(reflect.TypeOf([]byte{})) || t.In(0).AssignableTo(reflect.TypeOf(string("")))) {
		return nil
	}
	return fmt.Errorf("Update method must have signature func(string) []error or func([]byte) []error. Has %T", update.Interface())
}

// NodeStringOrBytesUpdate calls Update on a NodeStringOrBytes.
// It may panic if NodeStringOrBytes has a incorrect or missing Update method
func NodeStringOrBytesUpdate(nsb NodeStringOrBytes, b []byte) []error {
	m := reflect.ValueOf(nsb).MethodByName("Update")
	var v interface{} = b
	if m.Type().In(0).AssignableTo(reflect.TypeOf(string(""))) {
		v = string(b)
	}
	return m.Call([]reflect.Value{reflect.ValueOf(v)})[0].Interface().([]error)
}
