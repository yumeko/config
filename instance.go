// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

// Instance is a NodeNil with NodeNil children.
// Instance should be a root node in most cases.
type Instance struct {
	Children []NodeNil
}

func NewInstance(children ...NodeNil) *Instance {
	return &Instance{
		Children: children,
	}
}

func (i *Instance) Attach(n NodeNil) {
	i.Children = append(i.Children, n)
}
func (i *Instance) Reload() (errs []error) {
	for _, c := range i.Children {
		errs = append(errs, c.Reload()...)
	}
	return
}
