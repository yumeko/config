// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

func NewString(value string, children ...NodeString) *String {
	return &String{
		Val:      value,
		Children: children,
	}
}

//String is a NodeNil with NodeString children.
type String struct {
	Val      string
	Children []NodeString
	sent     bool
}

//Change changes the string value. Reload must be called for it to take effect
func (i *String) Change(newval string) {
	i.Val = newval
	i.sent = false
}
func (i *String) Attach(n NodeString) {
	i.Children = append(i.Children, n)
}
func (i *String) Reload() (errs []error) {
	if !i.sent {
		for _, c := range i.Children {
			errs = append(errs, c.Update(i.Val)...)
		}
		i.sent = true
	}
	for _, c := range i.Children {
		errs = append(errs, c.Reload()...)
	}
	return
}
