// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

import (
	"fmt"
	"strings"

	"gitlab.com/yumeko/errors"
)

type Logger struct {
	Context interface{}
	Level   errors.Level
}

func NewLogger(name string) *Logger {
	return &Logger{
		Context: name,
	}
}

func (l *Logger) Log(level errors.Level, info ...interface{}) {
	if l.Level >= level {
		if l.Context != nil {
			oinfo := info
			info = make([]interface{}, len(info)+1)
			info[0] = l.Context
			copy(info[1:], oinfo)
		}
		errors.Log(info)
	}
}

func (l *Logger) Error(info ...interface{}) {
	l.Log(errors.LevelError, info)
}

func (l *Logger) Warn(info ...interface{}) {
	l.Log(errors.LevelWarn, info)
}

func (l *Logger) Access(info ...interface{}) {
	l.Log(errors.LevelAccess, info)
}

func (l *Logger) Info(info ...interface{}) {
	l.Log(errors.LevelInfo, info)
}

func (l *Logger) Debug(info ...interface{}) {
	l.Log(errors.LevelDebug, info)
}

func (l *Logger) Reload() []error {
	return nil
}

func (l *Logger) Update(v string) []error {
	v = strings.ToLower(v)
	switch v {
	case "error":
		l.Level = errors.LevelError
	case "warn":
		l.Level = errors.LevelWarn
	case "access":
		l.Level = errors.LevelAccess
	case "info":
		l.Level = errors.LevelInfo
	case "debug":
		l.Level = errors.LevelDebug
	default:
		return []error{fmt.Errorf("\"%v\" is not one of [Error,Warn,Access,Info,Debug]", v)}
	}
	return nil
}
