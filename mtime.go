// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package config

import "os"

func NewMTime(children ...NodeString) *MTime {
	return &MTime{
		Children:children,
	}
}

// MTime is a NodeString with NodeString children. Both strings are the file name.
// Child updates is only called when the mtime is different
type MTime struct {
	Children []NodeString
	name     string
	mtime    int64
}

func (i *MTime) Attach(n NodeString) {
	i.Children = append(i.Children, n)
}

func (i *MTime) Reload() (errs []error) {
	fi, err := os.Stat(i.name)
	mtime := int64(0)
	if err == nil {
		mtime = fi.ModTime().Unix()
	}
	if mtime != i.mtime {
		for _, c := range i.Children {
			errs = append(errs, c.Update(i.name)...)
		}
	}
	for _, c := range i.Children {
		errs = append(errs, c.Reload()...)
	}
	return
}

func (i *MTime) Update(finame string) (errs []error) {
	i.name = finame
	fi, err := os.Stat(finame)
	if err == nil {
		i.mtime = fi.ModTime().Unix()
	} else {
		i.mtime = 0
	}
	for _, c := range i.Children {
		errs = append(errs, c.Update(finame)...)
	}
	return
}
